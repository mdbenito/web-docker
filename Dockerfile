FROM alpine:latest
MAINTAINER Miguel de Benito <m.debenito.d@gmail.com>

RUN apk add --no-cache nodejs make bash git

RUN npm install -g aerobatic-cli
RUN npm install -g cssnano-cli
RUN npm install -g html-minifier-cli
RUN npm install -g google-closure-compiler-js
