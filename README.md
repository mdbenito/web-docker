# docker-mdbenito

A minimal image based on Alpine. It is available at
[hub.docker.com/r/mdbenito/web/](https://hub.docker.com/r/mdbenito/web/).

It contains `git`, `make`, `node.js` and the node packages required to
build [my site](https://mdbenito.aerobatic.io) and deploy it
on [aerobatic](https://www.aerobatic.com):

 * [cssnano-cli](https://www.npmjs.com/package/cssnano-cli)
 * [html-minifier-cli](https://www.npmjs.com/package/html-minifier)
 * [google-closure-compiler-js](https://www.npmjs.com/package/google-closure-compiler-js)
 * [aerobatic-cli](https://www.npmjs.com/package/aerobatic-cli)

The image is used in a Bitbucket pipeline configured in
my [site's repo](https://bitbucket.org/mdbenito/web).

## Building

This should already be automated with DockerHub using a webhook in the
Bitbucket repo. If you need to do it locally for some reason, then use:

```
docker build -t mdbenito/web .
docker login
docker push mdbenito/web
```

## License

These files are licensed
under [CC BY](https://creativecommons.org/licenses/by/4.0/).
